/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class ARItemSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["main", "sheet", "item"],
      width: 520,
      height: 480,
      tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
    });
  }

  /** @override */
  get template() {
    const path = "systems/akkadianrhythms/templates/item";

    return `${path}/item-sheet.html`;
    //return `${path}/${this.item.data.type}-sheet.html`;
  }

  /* -------------------------------------------- */

  /** @override */
  getData() {
    // Retrieve base data structure.
    const context = super.getData();

    // Use a safe clone of the item data for further operations.
    const itemData = context.item;

    // Add the actor's data to context.data for easier access, as well as flags.
    context.system = itemData.system;
    context.flags = itemData.flags;

    return context;
  }

  /* -------------------------------------------- */

  /** @override */
  // setPosition(options = {}) {
  //   const position = super.setPosition(options);
  //   const sheetBody = this.element.find(".sheet-body");
  //   const bodyHeight = position.height - 192;
  //   sheetBody.css("height", bodyHeight);
  //   return position;
  // }
  

/** @override */
	activateListeners(html) {
		super.activateListeners(html);

		html.find('.rollable').click(this._onRoll.bind(this));
	}
  /* -------------------------------------------- */

	_onRoll(event) {
		event.preventDefault();
		const element = event.currentTarget;
		const dataset = element.dataset;		

		if (dataset.roll) {
			let cl = new ChatLog();
			let msg = ChatMessage.create(
			{
				content: dataset.roll
			});
			
			cl.postOne(msg, true);
		}
	}
}
