// Import Modules
import { ARActor } from "./actor/actor.js";
import { ARActorSheet } from "./actor/actor-sheet.js";
import { ARItem } from "./item/item.js";
import { ARItemSheet } from "./item/item-sheet.js";
import { _getInitiativeFormula } from "./encounters.js";

Hooks.once('init', async function() {
	game.ar = {
		ARActor,
		ARItemSheet
	};

	/**
	* Set an initiative formula for the system
	* @type {String}
	*/
	CONFIG.Combat.initiative = {
		formula: "0",
		decimals: 2
	};

	CONFIG.Actor.entityClass = ARActor;
	CONFIG.Item.entityClass = ARItem;

	Combat.prototype._getInitiativeFormula = _getInitiativeFormula;

	Actors.unregisterSheet("core", ActorSheet);
	Actors.registerSheet("akkadianrhythms", ARActorSheet, { makeDefault: true });
	Items.unregisterSheet("core", ItemSheet);
	Items.registerSheet("akkadianrhythms", ARItemSheet, { makeDefault: true });
  
	Handlebars.registerHelper('ne', function(val1, val2) {
		return val1 !== val2;
	});

	Handlebars.registerHelper('nnoe', function(val1) {
		if (val1 === null || val1 === undefined)
			return false;
		
		if (val1 === '')
			return false;

		return true;
	});
});
