/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class ARActorSheet extends ActorSheet {

	/** @override */
	static get defaultOptions() {
		return mergeObject(super.defaultOptions, {
		  classes: ["main", "sheet", "actor"],
		  template: "systems/akkadianrhythms/templates/actor/actor-sheet.html",
		  width: 600,
		  height: 600,
		  tabs: [{ navSelector: ".sheet-tabs", contentSelector: ".sheet-body", initial: "description" }]
		});
	}

	/** @override */
	getData() {
		// const data = super.getData();
		// data.dtypes = ["String", "Number", "Boolean"];

		// this._prepareCharacterItems(data);

		// return data;

		const context = super.getData();

		// Use a safe clone of the actor data for further operations.
		const actorData = this.actor.toObject(false);

		// Add the actor's data to context.system for easier access, as well as flags.
		context.system = actorData.system;
		context.flags = actorData.flags;
	
		this._prepareCharacterItems(context);
		
		// Add roll data for TinyMCE editors.
		context.rollData = context.actor.getRollData();
	
		return context;
	}


	/** @override */
	activateListeners(html) {
		super.activateListeners(html);

		html.find('.rollable').click(this._onRoll.bind(this));
		
		html.find('.rollable-stat').click(this._onStatRoll.bind(this));

		// Everything below here is only needed if the sheet is editable
		//if (!this.options.editable) return;
		
		// Add Inventory Item
		html.find('.item-create').click(this._onItemCreate.bind(this));

		// Update Inventory Item
		html.find('.item-edit').click(event => {
			event.preventDefault();
			const element = event.currentTarget;
			const dataset = element.dataset;
		  
			const item = this.actor.items.get(dataset.itemid);
			item.sheet.render(true);
		});

		// Delete Inventory Item
		html.find('.item-delete').click(event => {
			event.preventDefault();
			const element = event.currentTarget;
			const dataset = element.dataset;
		  
			const item = this.actor.items.get(dataset.itemid);
			item.delete();
		    li.slideUp(200, () => this.render(false));
		});
	}

	_onRoll(event) {
		event.preventDefault();
		const element = event.currentTarget;
		const dataset = element.dataset;

		if (dataset.roll) {
			let cl = new ChatLog();
			let msg = ChatMessage.create(
			{
				content: dataset.roll,
				speaker: ChatMessage.getSpeaker({ actor: this.actor })
			});
			
			cl.postOne(msg, true);
		}
	}
	
	_onStatRoll(event) {
		event.preventDefault();
		const element = event.currentTarget;
		const dataset = element.dataset;

		if (dataset.statval) {
			let statval = Number(dataset.statval);
			let modval = 0;
			if (!isNaN(dataset.modval)) {
				modval = Number(dataset.modval);
			}
			let stattotal = statval + modval;
			
			let rollcmd = 'Skill Check - ' + dataset.statname + ' ' + statval + ' + (' + modval + ')' + ': ';
			for (var i = 0; i < stattotal; i++) {
				rollcmd = rollcmd + '[[d6]]';
			}

			let cl = new ChatLog();
			let msg = ChatMessage.create(
			{
				content: rollcmd,
				speaker: ChatMessage.getSpeaker({ actor: this.actor })
			});
			
			cl.postOne(msg, true);
		}
	}
	
	_onItemCreate(event) {
		event.preventDefault();
		const header = event.currentTarget;
		// Get the type of item to create.
		const type = header.dataset.type;
		// Grab any data associated with this control.
		const data = duplicate(header.dataset);
		// Initialize a default name.
		const name = `New ${type.capitalize()}`;
		// Prepare the item object.
		const itemData = {
		  name: name,
		  type: type,
		  data: data
		};
		// Remove the type from the dataset since it's in the itemData.type prop.
		delete itemData.system["type"];

		// Finally, create the item!
		return this.actor.createOwnedItem(itemData);
	}
	
	/**
   * Organize and classify Items for Character sheets.
   *
   * @param {Object} actorData The actor to prepare.
   *
   * @return {undefined}
   */
	_prepareCharacterItems(context) {
		const actorData = context.system;

		// Initialize containers.
		const weapons = [];
		const armor = [];
		const equipment = [];
		const artifacts = [];
		const skillsets = [];
		const powers = [];
		let skillactions = [];

		for (let i of context.items) {
			let item = i.system;
			i.img = i.img || DEFAULT_TOKEN;

			if (i.type === 'weapon') {
				weapons.push(i);
			}
			else if (i.type === 'armor') {
				armor.push(i);
			}
			else if (i.type === 'equipment') {
				equipment.push(i);
			}
			else if (i.type === 'artifact') {
				artifacts.push(i);
			}
			else if (i.type === 'skillset') {
				skillsets.push(i);
			}
			else if (i.type === 'power') {
				powers.push(i);
			}
			else if (i.type === 'skillaction') {
				skillactions.push(i);
			}
		}
		
		for (let sa of skillactions) {
			let skillset = skillsets.find(ss => ss.name === sa.system.skillsetname);
			
			if (skillset !== undefined) {
				if (sa.system.startingaction) {
					skillset.system.startingactions.push(sa);
				} else {
					skillset.system.advancedactions.push(sa);
				}
			}
		}

		// Assign and return
		actorData.weapons = weapons;
		actorData.armor = armor;
		actorData.equipment = equipment;
		actorData.artifacts = artifacts;
		actorData.skillsets = skillsets;
		actorData.powers = powers;
	}
}