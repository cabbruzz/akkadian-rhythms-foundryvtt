/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class ARActor extends Actor {

  prepareData() {
    super.prepareData();

    // const actorData = this;
    // const systemData = actorData.system;
    // const flags = actorData.flags.boilerplate || {};

    // // Make separate methods for each Actor type (character, npc, etc.) to keep
    // // things organized.
    // if (actorData.type === 'character') this._prepareCharacterData(actorData);
    // else if (actorData.type === 'monster') this._prepareMonsterData(actorData);
    // else if (actorData.type === 'npc') this._prepareNPCData(actorData);
  }

  // _prepareCharacterData(actorData) {
  //   const data = actorData.system;
  // }

  // _prepareMonsterData(actorData) {
  //   const data = actorData.system;
  // }

  // _prepareNPCData(actorData) {
  //   const data = actorData.system;
  // }
}
