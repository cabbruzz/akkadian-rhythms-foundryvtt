/**
 * Use Move as Initiative score. 
 * 
 * For easier automation in Foundry we will use dex and agl as tie breakers.
 * Normal play lets players win ties, and asks them to assign order on ties with other players.
 */
export const _getInitiativeFormula = function(combatant) {
    let actor = combatant.actor;

    if (!actor)
        return "1";

    let data = actor.data.data;

    let initValue = data.stats.move.value + Number(data.stats.move.mod);

    //tie breaker with agility
    let aglInit = (data.stats.agl.value + Number(data.stats.agl.mod)) * 0.1;
    if (aglInit > 0.9) {
        aglInit = 0.9;
    }

    //tie breaker with dex
    let dexInit = (data.stats.dex.value + Number(data.stats.dex.mod)) * 0.01;
    if (dexInit > 0.09) {
        dexInit = 0.09;
    }

    initValue += aglInit + dexInit;

    return initValue.toString();
}